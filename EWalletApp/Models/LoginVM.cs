﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EWalletApp.Models
{
    public class LoginVM
    {
        [Required]
        [StringLength(100, MinimumLength = 8, ErrorMessage = "Minimum 8 characters.")]
        public string Username { get; set; }
        [Required]
        [StringLength(100, MinimumLength = 8, ErrorMessage = "Minimum 8 characters.")]
        public string UserPass { get; set; }
    }
}

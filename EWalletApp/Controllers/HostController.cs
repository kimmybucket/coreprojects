﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using EWalletApp.Models;

namespace EWalletApp.Controllers
{
    [AllowAnonymous]
    [Route("host")]
    public class HostController : Controller
    {
        [Route("login")]
        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public IActionResult HostLogin(LoginVM model)
        {
            if (ModelState.IsValid)
            {
                return View("Login");
            }
            else
            {
                ModelState.AddModelError("", "Unknown error.");
                return View("Login");
            }

        }
    }
}